package gr.uoa.di.server.main;

public class Shutdown implements Runnable {

//	private Server server;
//	
//	public  Shutdown (Server serv){
//		server = serv;
//	}

	@Override
	public void run(){
		System.out.println("\nServer is to be terminated\n");
		
		///////////////////////////////////////////
		/// Things to take care before shutdown ///
		///////////////////////////////////////////

		Server.stopServer();

	}
	
	
	
	
	
}
