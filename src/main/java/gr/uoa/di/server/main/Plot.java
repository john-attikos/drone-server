package gr.uoa.di.server.main;

import java.util.ArrayList;

import java.util.concurrent.TimeUnit;

import gr.uoa.di.server.db.dbClasses.Attribute;
import gr.uoa.di.server.db.dbClasses.Image;
import gr.uoa.di.server.db.dbClasses.UserHasAttributes;
import gr.uoa.di.server.db.dbOperations.DbAttributeOperations;
import gr.uoa.di.server.db.dbOperations.DbHistoryOperations;
import gr.uoa.di.server.db.dbOperations.DbUserHasAttributesOperations;
import gr.uoa.di.server.db.dbOperations.DbUserOperations;


public class Plot {
	
	DbUserOperations usersOperations = new DbUserOperations();
	DbHistoryOperations historyOperations = new DbHistoryOperations();
	DbAttributeOperations attrOperations = new DbAttributeOperations();
	DbUserHasAttributesOperations uhaOperations = new DbUserHasAttributesOperations();
	
	public void plot1() {

		usersOperations.deleteAllUsers();
		attrOperations.deleteAllAttributes();



		usersOperations.insertUser("user1", "attikos", "a@a.com");
		usersOperations.insertUser("user2", "sokitta", "a@a.com");
		
		attrOperations.insertAttribute(Attribute.TEMPERATURE);
		attrOperations.insertAttribute(Attribute.WIND_DIRECTION);
		attrOperations.insertAttribute(Attribute.WATERING);

		uhaOperations.insertUserHasAttributes("user1", Attribute.TEMPERATURE);
		uhaOperations.insertUserHasAttributes("user1", Attribute.WATERING);

		uhaOperations.insertUserHasAttributes("user2", Attribute.WIND_DIRECTION);
		uhaOperations.insertUserHasAttributes("user2", Attribute.WATERING);
		
		System.out.println("\n\nabout to insert history.....");
		
		
		historyOperations.insertHistory("user1", Attribute.TEMPERATURE, "35");
		historyOperations.insertHistory("user1", Attribute.WATERING, "30");
		historyOperations.insertHistory("user2", Attribute.WIND_DIRECTION, "north");

	}
	
	public void plot2()
	{

		usersOperations.deleteAllUsers();
		attrOperations.deleteAllAttributes();


		usersOperations.insertUser("user1", "attikos", "a@a.com");
		attrOperations.insertAttribute("temperature");

		
		uhaOperations.insertUserHasAttributes("admin", Attribute.TEMPERATURE);
		uhaOperations.insertUserHasAttributes("admin", Attribute.WATERING);
		uhaOperations.insertUserHasAttributes("admin", Attribute.HUMIDITY);
		historyOperations.insertHistory("admin", Attribute.TEMPERATURE, "35");
		historyOperations.insertHistory("admin", Attribute.TEMPERATURE, "11");
		historyOperations.insertHistory("admin", Attribute.WATERING, "123");


		uhaOperations.insertUserHasAttributes("user1", Attribute.TEMPERATURE);
		historyOperations.insertHistory("user1", Attribute.TEMPERATURE, "35");
		historyOperations.insertHistory("user1", Attribute.TEMPERATURE, "123");


		
	}
	 
	//best plot
	public void plot3()
	{
		
		usersOperations.deleteAllUsers();
		attrOperations.deleteAllAttributes();

		
		//////////////////////////////////
		// INSERT admin and his history //
		usersOperations.insertUser("admin", "admin", "sdi0700308@di.uoa.gr");
		
		historyOperations.insertHistory("admin", Attribute.TEMPERATURE, "33");
		historyOperations.insertHistory("admin", Attribute.TEMPERATURE, "44");
		historyOperations.insertHistory("admin", Attribute.TEMPERATURE, "55");

		historyOperations.insertHistory("admin", Attribute.WIND_DIRECTION, "north");
		historyOperations.insertHistory("admin", Attribute.WIND_DIRECTION, "south");
		historyOperations.insertHistory("admin", Attribute.WIND_DIRECTION, "west-south");

		historyOperations.insertHistory("admin", Attribute.AIR_PRESSURE, "50");
		historyOperations.insertHistory("admin", Attribute.AIR_PRESSURE, "11");
		historyOperations.insertHistory("admin", Attribute.AIR_PRESSURE, "20");
		historyOperations.insertHistory("admin", Attribute.AIR_PRESSURE, "30");

		historyOperations.insertHistory("admin", Attribute.CLOUDINESS, "high");
		historyOperations.insertHistory("admin", Attribute.CLOUDINESS, "normal");

		historyOperations.insertHistory("admin", Attribute.WIND_SPEED, "5");
		historyOperations.insertHistory("admin", Attribute.WIND_SPEED, "1");
		historyOperations.insertHistory("admin", Attribute.WIND_SPEED, "2");
		historyOperations.insertHistory("admin", Attribute.WIND_SPEED, "2");
		historyOperations.insertHistory("admin", Attribute.WIND_SPEED, "11");

		//////////////////////////////////
		// INSERT user1 and his history //
		usersOperations.insertUser("user1", "1111", "sdi0700308@di.uoa.gr");
		
		historyOperations.insertHistory("user1", Attribute.TEMPERATURE, "33");
		historyOperations.insertHistory("user1", Attribute.TEMPERATURE, "44");
		historyOperations.insertHistory("user1", Attribute.TEMPERATURE, "55");

		historyOperations.insertHistory("user1", Attribute.WIND_DIRECTION, "north");
		historyOperations.insertHistory("user1", Attribute.WIND_DIRECTION, "south");

		historyOperations.insertHistory("user1", Attribute.AIR_PRESSURE, "50");
		historyOperations.insertHistory("user1", Attribute.AIR_PRESSURE, "10");
		
	}
	
	public void plot4()
	{
		Image image = new Image();
		
		image.setEncoded(image.imageToString("logo_temperature.png"));
		
	}

	public void plot5()
	{
		usersOperations.deleteAllUsers();
		attrOperations.deleteAllAttributes();
		
		usersOperations.insertUser("admin", "admin", "sdi0700308@di.uoa.gr");
		
		historyOperations.insertHistory("admin", Attribute.TEMPERATURE, "33 celsius");
		historyOperations.insertHistory("admin", Attribute.TEMPERATURE, "29 celsius");
		historyOperations.insertHistory("admin", Attribute.TEMPERATURE, "30 celsius");
		historyOperations.insertHistory("admin", Attribute.TEMPERATURE, "28 celsius");
		historyOperations.insertHistory("admin", Attribute.TEMPERATURE, "32 celsius");
		historyOperations.insertHistory("admin", Attribute.AIR_PRESSURE, "10 atm");
		historyOperations.insertHistory("admin", Attribute.HUMIDITY, "5 %");
		historyOperations.insertHistory("admin", Attribute.CLOUDINESS, "high");
		historyOperations.insertHistory("admin", Attribute.WATERING, "high");
		historyOperations.insertHistory("admin", Attribute.WIND_DIRECTION, "south-east");
		historyOperations.insertHistory("admin", Attribute.WIND_SPEED, "6 Beaufort");

//		MyLogger.printd("");
//		MyLogger.printd("");
//		ArrayList<String> userImages = Image.getUserDroneImages("user1");
//
//		for (int i=0; i<userImages.size(); i++)
//		{
//			MyLogger.printd("user has image: " + userImages.get(i));
//		}
		
		
	}

}
