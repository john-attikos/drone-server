package gr.uoa.di.server.main;


// NOTE: Dummy class since no drone exists
public class DummyDrone {
    private int batteryState 	 = 0;
    private int distanceFromBase = 0;
    private int flightTime 		 = 0;

    
    public int getDistanceFromBase() {
        return distanceFromBase;
    }

    public int getFlightTime() {
        return flightTime;
    }

    public int getBatteryState() {
		return batteryState;
	}
    
    public void setDistanceFromBase(int distanceFromBase) {
		this.distanceFromBase = distanceFromBase;
	}
    
    public void setBatteryState(int batteryState) {
		this.batteryState = batteryState;
	}
    
    public void setFlightTime(int flightTime) {
		this.flightTime = flightTime;
	}
    
    
}
