package gr.uoa.di.server.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class PropertyFile {

	// Let's read only once from the disk
	// and keep everything on the below vars
	// rather than read in every getProperty call...
	public static Properties netProperties;
	public static Properties dbProperties;
	
	
	private PropertyFile()
	{
		netProperties = LoadNetworkProperties();
		dbProperties = LoadDbProperties();
	}

	public static void LoadProperties()
	{
		// LoadProperties() will initialize the static 
		// variables/methods which are accessed by everywhere
		new PropertyFile();
	}
	
	public static String getBaseURI()
	{
		return getServerIP() + ":" + getServerPort() + getServerBasePath() ;
	}
	
	
    public static String getServerIP()
    {
    	return netProperties.getProperty("serverIP");
    }


    public static String getServerPort()
    {
    	return netProperties.getProperty("serverPort");
    }


    public static String getServerBasePath()
    {    	
    	return netProperties.getProperty("serverBasePath");
    }

    
    public static String getServerResourcePath()
    {
    	return netProperties.getProperty("serverResourcePath");
    }
    
    
    public static String getServerTimeout()
    {
    	return netProperties.getProperty("serverTimeout");
    }
    
    
    public static String getServerTimeoutCheckFreq()
    {
    	return netProperties.getProperty("serverTimeoutCheckFreq");
    }
    
    
    public static String getDbUrl()
    {
    	return dbProperties.getProperty("URL");
    }

    
    public static String getDbDriver()
    {
    	return dbProperties.getProperty("DRIVER");
    }

    
    public static String getDbUsername()
    {
    	return dbProperties.getProperty("USERNAME");
    }

    
    public static String getDbPassword()
    {
    	return dbProperties.getProperty("PASSWORD");
    }

    
    public static String getDbName()
    {
    	return dbProperties.getProperty("DBNAME");
    }

    
    private Properties LoadNetworkProperties() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("properties/networking.properties");
			// load a properties file
			prop.load(input);
			return prop;
		} catch (IOException ex) {
			System.out.println("[WARN] dbConfig.properties does not exist, will try to restore an reopen...");
			restoreNetProperties();
			try {
				// Now that it has been restored, let's try to reopen it
				input = new FileInputStream("properties/networking.properties");
				prop.load(input);
				System.out.println("Property file successfuly restored!");
				return prop;
			} catch (IOException ex2) {
				System.out.println("Property file could not be restored...");
				ex.printStackTrace();
			}
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;

	}

    
    private Properties LoadDbProperties() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("properties/dbConfig.properties");
			// load the properties file
			prop.load(input);
			return prop;
		} catch (IOException ex) {
			System.out.println("[WARN] dbConfig.properties does not exist, will try to restore an reopen...");
			restoreDbProperties();
			try {
				// Now that it has been restored, let's try to reopen it
				input = new FileInputStream("properties/dbConfig.properties");
				prop.load(input);
				System.out.println("Property file successfuly restored!");
				return prop;
			} catch (IOException ex2) {
				System.out.println("Property file could not be restored...");
				ex.printStackTrace();
			}
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return prop;
	}
    
    public static void restoreDbProperties()
    {
    	File propertiesFile = new File("properties/dbConfig.properties");
    	try {
    		System.out.println("file 'properties/dbConfig.properties' does not exists...");
    		propertiesFile.createNewFile();
    		FileWriter buffer = new FileWriter("properties/dbConfig.properties");
    		buffer.write("URL=jdbc:mysql://localhost:3306/" + "\n");
    		buffer.write("DRIVER=com.mysql.jdbc.Driver" + "\n");
    		buffer.write("USERNAME=root" + "\n");
    		buffer.write("PASSWORD=1234" + "\n");
    		buffer.write("DBNAME=air_drone_db" + "\n");
    		buffer.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    }

    
    public static void restoreNetProperties()
    {
    	File propertiesFile = new File("properties/dbConfig.properties");
    	try {
    		System.out.println("file 'properties/dbConfig.properties' does not exists...");
    		propertiesFile.createNewFile();
    		FileWriter buffer = new FileWriter("properties/networking.properties");
    		buffer.write("http://0.0.0.0" + "\n");
    		buffer.write("serverPort=8081" + "\n");
    		buffer.write("serverBasePath=/myapp/" + "\n");
    		buffer.write("serverResourcePath=gr.uoa.di.server");
    		buffer.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
    }

}
