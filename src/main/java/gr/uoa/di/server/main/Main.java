package gr.uoa.di.server.main;

import java.io.IOException;

import gr.uoa.di.server.db.dbOperations.DbCreation;
import gr.uoa.di.server.db.dbOperations.DbUserOperations;


public class Main {

	public static void main(String[] args) throws IOException,InterruptedException {

		PropertyFile.LoadProperties();
		DbCreation.createDb();
		DbUserOperations.insertAdmin();
		Server.createServer();
		
		// Actions to be made on server's shutdown (CTRL-C)
		Shutdown shutdown = new Shutdown();
		Thread termination = new Thread(shutdown);
    	Runtime.getRuntime().addShutdownHook(termination);

    	
		///// TESTING
		///// TESTING
		
    	
    	Plot plot = new Plot();
    	
    	
		
    	// Plot3 ads 2 users. 
    	// You can try to login from android with admin (empty fields) or with: 
    	//	un: user1
    	//  pw: 1111
    	// to check the displayed attributes change according to user

//    	plot.plot3(); // best plot
    	plot.plot5();
		
		///// TESTING
		///// TESTING

		
		System.out.println("\nServer up and running on: " 
					+ PropertyFile.getServerIP() + ":"
					+ PropertyFile.getServerPort()	+ "\n(Hit ctrl-c to shut server down)\n");
		
        Thread.currentThread().join();
	}

}
