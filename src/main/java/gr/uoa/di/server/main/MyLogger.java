package gr.uoa.di.server.main;

public class MyLogger {

	private static final String debug = "[DEBUG] ";
	private static final String info = "[INFO] ";
	private static final String warn = "[WARN] ";
	private static final String error = "[ERROR] ";

	public static void printd(String message) 
	{
		System.out.println(debug + message);		
	}
	
	public static void printi(String message) 
	{
		System.out.println(info + message);		
	}
	
	public static void printw(String message) 
	{
		System.out.println(warn + message);		
	}
	
	public static void printe(String message) 
	{
		System.out.println(error + message);		
	}
	
}