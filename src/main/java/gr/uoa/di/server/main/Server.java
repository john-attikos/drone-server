package gr.uoa.di.server.main;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

/////////////////////////////////////////
/// Let's keep everything here static ///
/////////////////////////////////////////


public class Server {

	final ResourceConfig rc;
	public static String uri = PropertyFile.getServerIP();
	public static String port = PropertyFile.getServerPort();
	public static String application = PropertyFile.getServerBasePath();
	public static final URI BASE_URI = UriBuilder.fromUri(uri+":"+port+application).build();
	private static HttpServer httpServer;	

	private static Server instance = null;
	
	// Let's keep only one instance of the server
	private Server()
	{
		rc = new ResourceConfig().packages(PropertyFile.getServerResourcePath());
		httpServer = GrizzlyHttpServerFactory.createHttpServer(BASE_URI, rc);		
	}


    public static synchronized Server createServer() 
    {
    	if( instance == null )
			instance = new Server();
		return instance;
	}


    public static synchronized HttpServer getServer()
    {
    	createServer();
    	return httpServer;    	
    }
    

    public static synchronized void stopServer() 
	{
		httpServer.stop();
	}

}

