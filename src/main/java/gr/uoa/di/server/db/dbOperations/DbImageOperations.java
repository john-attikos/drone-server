package gr.uoa.di.server.db.dbOperations;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import gr.uoa.di.server.db.dbClasses.Image;
import gr.uoa.di.server.main.MyLogger;

public class DbImageOperations{


	public static boolean checkImageExistance(String name) {
		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;
		try {
			//checking if Name exists
			String query = "select * from Images where file_name = ?";
			st = conn.prepareStatement(query);
			st.setString(1, name);
			res = st.executeQuery();
			if (res.next())
				return true;
			else
				return false;
    	}
    	catch (SQLException e) {
			e.printStackTrace();
    	} finally {
    		try {
				conn.close();
    		} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
		return false;
	}

		
	
	public void insertImageRecord(String name) {
		if (checkImageExistance(name))
			return;
		

		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;


		try {
			st = conn.prepareStatement("INSERT INTO Images (file_name) VALUES (?)");
			st.setString(1, name);
			st.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public ArrayList<Image> getAllImages() {
		ArrayList<Image> images = new ArrayList<Image>();
	
		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;
		try {
			//checking if UserName exists
			String query = "select * from Images";
			st = conn.prepareStatement(query);
			res = st.executeQuery();
			while (res.next()) {
				Image image = new Image();
				image.setFileName(res.getString("file_name"));
				images.add(image);
			}
		
		}
    	catch (SQLException e) {
			e.printStackTrace();
    	} finally {
    		try {
				conn.close();
    		} catch (SQLException e) {
				e.printStackTrace();
    		} finally {
    			try {
    				conn.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}
    		}
    	}
		return images;
	}

	
	// For debugging
	public void printAllImages() {
		
		ArrayList<Image> images = getAllImages();

		System.out.println("+++++++++++images");
		for (int i=0; i<images.size(); i++) {
			System.out.println("fileName:"+images.get(i).getFileName());
			
		}
	}

	
	public void deleteImage(String name) {
		
    	DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();

    	MyLogger.printi("> Deleting image: " + name);
		
    	 File imageFile = new File("drone-photos/" + name);

    	 if (imageFile.exists()) {
    		 imageFile.delete();        
    	 }
    	
    	try {
			st = conn.prepareStatement("DELETE FROM Images WHERE file_name = ?");
	    	st.setString(1, name);
	    	st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
    	
	}

	public void deleteAllImages() {
				
		ArrayList<Image> images = getAllImages();

		MyLogger.printi("Deleting all images");

		for (int i=0; i<images.size(); i++) {
			deleteImage(images.get(i).getFileName());
			
		}
		
	}
	
}
