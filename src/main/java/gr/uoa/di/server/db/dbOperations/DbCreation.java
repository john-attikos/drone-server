package gr.uoa.di.server.db.dbOperations;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import gr.uoa.di.server.main.PropertyFile;

public class DbCreation {
	private static DbCreation instance = null;
    private String url; 				
    private String driver; 		
    private String userName;
    private String password; 	
    private String dbName;
        
    private DbCreation() {				
    	fixDbProperties();
    }
    
    public static synchronized DbCreation getInstance() {
	if(instance == null)
			instance = new DbCreation();
		return instance;
	}
	
    //////////////////////////////////////////////
    /// Returns a connection with MySql //////////
    public Connection connectWithMysql() {
    	Connection conn;
    	try {
    		Class.forName(driver).newInstance();
    		conn = DriverManager.getConnection(url, userName, password);
    	}
    	catch (Exception e){
    		System.out.println("*** Cannot connect with mysql ***");
    		e.printStackTrace();
    		conn=null;
    	}
    	return conn;
    }
    
    ////////////////////////////////////
    /// Returns a connection with DB ///
    public Connection connect() {
    	Connection conn;
    	try {
    		Class.forName(driver).newInstance();
    		conn = DriverManager.getConnection(url+dbName, userName, password);
    	}
    	catch (Exception e){
    		//System.out.println("***Cannot connect with database");
    		e.printStackTrace();
    		conn=null;
    	}
    	return conn;
    }

    public static void createDb() {
    	DbCreation sql = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = sql.connectWithMysql();

    	//////////////////////////////////////////
    	/// Creating database ////////////////////
    	try {
    		st = conn.prepareStatement("CREATE DATABASE IF NOT EXISTS air_drone_db");
    		st.executeUpdate();	    		
    	}
    	catch (SQLException e) {
    		System.out.println("*** Error while creating database air_drone_db ***");
    		return;
    	}
    	finally {
    		try { 
    			conn.close(); 
    		}
    		catch (SQLException e) { 
        		System.out.println("*** Cannot close connection with db ***");
    			e.printStackTrace(); 
    		}
    	}
    	////////////////////////
    	/// create DB tables ///
    	DbCreation.createDbTables();

    }
    
    public static void createDbTables() {
        DbCreation sql = DbCreation.getInstance();
        Connection conn;
        PreparedStatement st;
        conn = sql.connect();

        ////////////////////////////////////////
        /// Creating Tables ////////////////////
        try {
        	// System.out.println("Checking 'Users' table");
        	st = conn.prepareStatement("CREATE TABLE IF NOT EXISTS Users (username VARCHAR(30) NOT NULL PRIMARY KEY, password VARCHAR(30) NOT NULL, email VARCHAR(30) NOT NULL, loggedin VARCHAR(5))");
        	st.executeUpdate();
        	st.clearParameters();

        	
        	// System.out.println("Checking 'Attributes' table");
        	st = conn.prepareStatement("CREATE TABLE IF NOT EXISTS Attributes (name VARCHAR(30) NOT NULL PRIMARY KEY)");
        	st.executeUpdate();
        	st.clearParameters();
        	
        	// System.out.println("Checking 'Users_has_attributes' table");
        	st = conn.prepareStatement("CREATE TABLE IF NOT EXISTS Users_has_attributes (username VARCHAR(30), attribute VARCHAR(30), FOREIGN KEY(username) REFERENCES Users(username), FOREIGN KEY(attribute) REFERENCES Attributes(name))");
        	st.executeUpdate();
        	st.clearParameters();
        	
        	// System.out.println("Checking 'History' table");
        	st = conn.prepareStatement("CREATE TABLE IF NOT EXISTS History (id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, username VARCHAR(30), attribute VARCHAR(30), FOREIGN KEY(username) REFERENCES Users(username), FOREIGN KEY(attribute) REFERENCES Attributes(name), date VARCHAR(30), value VARCHAR(30))");
        	st.executeUpdate();
        	st.clearParameters();

        	// System.out.println("Checking 'Images' table");
        	st = conn.prepareStatement("CREATE TABLE IF NOT EXISTS Images (file_name VARCHAR(30) NOT NULL PRIMARY KEY)");
        	st.executeUpdate();
        	st.clearParameters();

        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        finally {
            try {
                conn.close();
            }
            catch (SQLException se) {
                System.out.println("*** Cannot close connection with db ***");
                se.printStackTrace();
            }
        }

    }
    private void fixDbProperties() {
    	
    	File propertiesFile = new File("properties/dbConfig.properties");
    	
		if( !propertiesFile.exists() ) 
		{
			PropertyFile.restoreDbProperties();
		}
		this.url = PropertyFile.getDbUrl();
		this.driver = PropertyFile.getDbDriver();
		this.userName = PropertyFile.getDbUsername();
		this.password = PropertyFile.getDbPassword();
		this.dbName = PropertyFile.getDbName();
    } 
}


