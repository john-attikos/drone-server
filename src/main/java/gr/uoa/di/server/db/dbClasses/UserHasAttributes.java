package gr.uoa.di.server.db.dbClasses;

public class UserHasAttributes {

	private String username;
	private String attribute;
	
	public void setUsername(String username) {
		this.username = username;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public String getUsername() {
		return username;
	}
	public String getAttribute() {
		return attribute;
	}
}
