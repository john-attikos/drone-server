package gr.uoa.di.server.db.dbClasses;

public class Attribute {

	private String name;
	
    public static String TEMPERATURE 	= "temperature";
    public static String HUMIDITY 	 	= "humidity";
    public static String WIND_SPEED 	= "wind speed";
    public static String WIND_DIRECTION = "wind direction";
    public static String WATERING 		= "watering";
    public static String AIR_PRESSURE 	= "air pressure";
    public static String CLOUDINESS 	= "cloudiness";

    public static String LOGO_TEMPERATURE 	 = "logo_temperature.png";
    public static String LOGO_HUMIDITY 	 	 = "logo_humidity.png";
    public static String LOGO_WIND_SPEED 	 = "logo_wind_speed.png";
    public static String LOGO_WIND_DIRECTION = "logo_wind_direction.png";
    public static String LOGO_WATERING 		 = "logo_watering.png";
    public static String LOGO_AIR_PRESSURE 	 = "logo_air_pressure.png";
    public static String LOGO_CLOUDINESS 	 = "logo_cloudiness.png";
    public static String LOGO_DEFAULT		 = "logo_default.png";

	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getLogoByAttribute(String attr) {

		String logo_name = new String(LOGO_DEFAULT);

		if ( attr.equals(TEMPERATURE) ) 
		{
			logo_name = LOGO_TEMPERATURE;
		}
		else if ( attr.equals(HUMIDITY) )
		{
			logo_name = LOGO_HUMIDITY;
		}
		else if ( attr.equals(WIND_SPEED) )
		{
			logo_name = LOGO_WIND_SPEED;
		}
		else if ( attr.equals(WIND_DIRECTION) )
		{
			logo_name = LOGO_WIND_DIRECTION;
		}
		else if ( attr.equals(WATERING) )
		{
			logo_name = LOGO_WATERING;
		}
		else if ( attr.equals(AIR_PRESSURE) )
		{
			logo_name = LOGO_AIR_PRESSURE;
		}
		else if ( attr.equals(CLOUDINESS) )
		{
			logo_name = LOGO_CLOUDINESS;
		}

		return logo_name;
	}

}
