package gr.uoa.di.server.db.dbOperations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import gr.uoa.di.server.db.dbClasses.User;
import gr.uoa.di.server.main.MyLogger;

public class DbUserOperations {
	
	public static void insertAdmin()
	{
		if (checkUserExistance("admin"))
			return;

		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;


		try {
			st = conn.prepareStatement("INSERT INTO Users (username, password, email, loggedin) VALUES (?, ?, ?, ?)");
			st.setString(1, "admin");
			st.setString(2, "admin");
			st.setString(3, "sdi0700308@di.uoa.gr");
			st.setString(4, "no");
			st.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public static boolean tryLogin(String user, String pass)
	{
		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;
		try {
			// validating user's credentials
			String query = "select * from Users where username = ? and password = ? ";
			st = conn.prepareStatement(query);
			st.setString(1, user);
			st.setString(2, pass);
			res = st.executeQuery();
			if (res.next())
				return true;
			else
				return false;
    	}
    	catch (SQLException e) {
			e.printStackTrace();
    	} finally {
    		try {
				conn.close();
    		} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
		return false;
	}
	

	public static boolean checkUserExistance(String username) {
		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;
		try {
			//checking if UserName exists
			String query = "select * from Users where username = ?";
			st = conn.prepareStatement(query);
			st.setString(1, username);
			res = st.executeQuery();
			if (res.next())
				return true;
			else
				return false;
    	}
    	catch (SQLException e) {
			e.printStackTrace();
    	} finally {
    		try {
				conn.close();
    		} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
		return false;
	}

		
	
	public void insertUser(String username, String password, String email) {
		if (checkUserExistance(username))
			return;

		MyLogger.printi("Adding user ( " + username + " - " + password + " - " + email + " )");
		
		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;


		try {
			st = conn.prepareStatement("INSERT INTO Users (username, password, email, loggedin) VALUES (?, ?, ?, ?)");
			st.setString(1, username);
			st.setString(2, password);
			st.setString(3, email);
			st.setString(4, "no");
			st.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public ArrayList<User> getAllUsers() {
		ArrayList<User> users = new ArrayList<User>();
	
		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;
		try {
			//checking if UserName exists
			String query = "select username,password,email,loggedin from Users";
			st = conn.prepareStatement(query);
			res = st.executeQuery();
			while (res.next()) {
				User user = new User();
				user.setUsername(res.getString("username"));
				user.setPassword(res.getString("password"));
				user.setEmail(res.getString("email"));
				user.setLoggedin(res.getString("loggedin"));
				users.add(user);
			}
		
		}
    	catch (SQLException e) {
			e.printStackTrace();
    	} finally {
    		try {
				conn.close();
    		} catch (SQLException e) {
				e.printStackTrace();
    		} finally {
    			try {
    				conn.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}
    		}
    	}
		return users;
	}

	
	// For debugging
	public void printAllUsers() {
		
		ArrayList<User> users = getAllUsers();

		for (int i=0; i<users.size(); i++) {
			System.out.println("+++++++++++");
			System.out.println("un:"+users.get(i).getUsername());
			System.out.println("pw:"+users.get(i).getPassword());
			System.out.println("em:"+users.get(i).getEmail());
			System.out.println("lo:"+users.get(i).getLoggedin());
			
		}
	}

	
	public void deleteUser(String username) {
		
    	DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
    	
    	MyLogger.printi("> Deleting user: " + username);

    	// Due to the relation of the tables, when deleting a user we should
    	// also delete the User_has_Attributes records which refer to him
    	DbUserHasAttributesOperations uha = new DbUserHasAttributesOperations();
		uha.deleteAllUsersHasAttrByUser(username);
		
		// same story goes for history
		DbHistoryOperations history = new DbHistoryOperations();
		history.deleteHistoryByUser(username);
		
    	try {
			st = conn.prepareStatement("DELETE FROM Users WHERE username = ?");
	    	st.setString(1, username);
	    	st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void deleteAllUsers() {
		ArrayList<User> users = getAllUsers();
		
		MyLogger.printi("Deleting all users");
		for (int i=0; i<users.size(); i++) {
			deleteUser(users.get(i).getUsername());			
		}
		
	}
	
}
