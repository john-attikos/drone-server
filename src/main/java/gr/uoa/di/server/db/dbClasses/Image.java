package gr.uoa.di.server.db.dbClasses;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;

import gr.uoa.di.server.main.MyLogger;
import sun.misc.BASE64Decoder;
 


public class Image {
	
	private static String imagesDir = "drone-photos/";
	private static String logosDir  = "attribute-logos/";
	
    private String fileName = new String();
    private String encoded = new String();

    public Image()
    {

    }

    public String getEncoded() {
        return encoded;
    }

    public void setEncoded(String encoded) {
        this.encoded = encoded;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }



    public BufferedImage decodeImage(String imageString) {

        BufferedImage image = null;
        byte[] imageByte;
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            imageByte = decoder.decodeBuffer(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }


    public void imageToFile()
    {
    	BufferedImage newImg;
    	
    	newImg = decodeImage(this.encoded);

    	// Get file extension (I decided to default it to JPG since this is camera's format)
    	String extension = "jpg";
    	int i = fileName.lastIndexOf('.');
    	if (i >= 0) {
    	    extension = fileName.substring(i+1);
    	}
    	
    	// All photos will begin with the owner's USERNAME
    	try {
			ImageIO.write(newImg, extension, new File(imagesDir + fileName));
		} catch (IOException e) {
			MyLogger.printe("Could not write image bytes to file");
			e.printStackTrace();
		}
    	
    }

    public String imageToString(String fileName)
    {
    	File file = new File(logosDir + fileName);

    	String imageDataString = new String();
    	try {            
            // Reading a Image file from file system
            FileInputStream imageInFile = new FileInputStream(file);
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);
 
            // Converting Image byte array into Base64 String
            imageDataString = encodeImage(imageData);
 
            // Converting a Base64 String into Image byte array
//            byte[] imageByteArray = decodeImage(imageDataString);
 
            // Write a image byte array into file system
//            FileOutputStream imageOutFile = new FileOutputStream(
//                    "/Users/jeeva/Pictures/wallpapers/water-drop-after-convert.jpg");
 
//            imageOutFile.write(imageByteArray);
 
            imageInFile.close();
//            imageOutFile.close();
 
//            System.out.println("Image Successfully Manipulated!");
        } catch (FileNotFoundException e) {
            System.out.println("Image not found\n" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the Image " + ioe);
        }
        
        return imageDataString;
    }

    
    public String droneImageToString(String fileName)
    {
    	File file = new File(imagesDir + fileName);

    	String imageDataString = new String();
    	try {            
            // Reading a Image file from file system
            FileInputStream imageInFile = new FileInputStream(file);
            byte imageData[] = new byte[(int) file.length()];
            imageInFile.read(imageData);
 
            // Converting Image byte array into Base64 String
            imageDataString = encodeImage(imageData);
 
            imageInFile.close();

    	} catch (FileNotFoundException e) {
            System.out.println("Image not found\n" + e);
        } catch (IOException ioe) {
            System.out.println("Exception while reading the Image " + ioe);
        }
        
        return imageDataString;
    }

    
    public static String encodeImage(byte[] imageByteArray) {
        return Base64.encodeBase64URLSafeString(imageByteArray);
    }
    
    
    public static ArrayList<String> getUserDroneImages(String user)
    {
    	ArrayList<String> userImagesNames = new ArrayList<>();
    	
    	File folder = new File(imagesDir);

    	///////////////////////////////////////////////////
    	// The trick here is that the name of all drone 
    	// images starts with the user's name, so this method
    	// will return only this user's drone images by 
    	// trying to match using a regular expression
    	
    	File[] listOfFiles = folder.listFiles();
    	for (int i=0; i < listOfFiles.length; i++) 
    	{
    		if (listOfFiles[i].getName().matches("^"+user+"_.*"))
    		{
    			userImagesNames.add(listOfFiles[i].getName());
    		}
    	}
    	
    	return userImagesNames;
    }

}
