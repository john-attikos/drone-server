package gr.uoa.di.server.db.dbOperations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import gr.uoa.di.server.db.dbClasses.UserHasAttributes;
import gr.uoa.di.server.main.MyLogger;

public class DbUserHasAttributesOperations {

	

	public static boolean checkUserHasAttrExists(String username, String attr) {
		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;
		try {
			//checking if UserName exists
			String query = "select * from Users_has_attributes where username = ? AND attribute = ?";
			st = conn.prepareStatement(query);
			st.setString(1, username);
			st.setString(2, attr);
			res = st.executeQuery();
			if (res.next())
				return true;
			else
				return false;
    	}
    	catch (SQLException e) {
			e.printStackTrace();
    	} finally {
    		try {
				conn.close();
    		} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
		return false;
	}

		
	
	public void insertUserHasAttributes(String username, String attr) {

		if ( !DbUserOperations.checkUserExistance(username) )
		{
			MyLogger.printw("Trying to add user-attribute pair but the user does not exists (pair: "+ username + "-" + attr + ")");
			return;
		}

		if (checkUserHasAttrExists(username, attr)) {
//			MyLogger.printw("Trying to insert a username-attribute pair which already exists");
			return;
		}

		// If we want to add a user-attribute pair, it is more efficient 
		// to automatically add here the attribute and not:
		// add the user - then the attribute - then the pair.
		
		// Note that attribute insertion checks if it already exists.
		DbAttributeOperations attrOp = new DbAttributeOperations();
		attrOp.insertAttribute(attr);
		
		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;

		try {
/*
			//checking if pair exists
			String query = "select username, attribute from Users_has_attributes where username = ? and attribute = ? ";
	    	PreparedStatement st2;
			st2 = conn.prepareStatement(query);
			st2.setString(1, username);
			st2.setString(2, attr);
			res = st2.executeQuery();

			boolean pairExists = false;
			
			if (res.next())
				pairExists = true;
			if (!pairExists)
				return;
*/						
			st = conn.prepareStatement("INSERT INTO Users_has_attributes (username, attribute) VALUES (?, ?)");
			st.setString(1, username);
			st.setString(2, attr);
			st.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public ArrayList<UserHasAttributes> getAllUsersHasAttr() {
		ArrayList<UserHasAttributes> uha = new ArrayList<UserHasAttributes>();
	
		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;
		try {
			//checking if UserName exists
			String query = "select * from Users_has_attributes";
			st = conn.prepareStatement(query);
			res = st.executeQuery();
			while (res.next()) {
				UserHasAttributes u = new UserHasAttributes();
				u.setUsername(res.getString("username"));
				u.setAttribute(res.getString("attribute"));
				uha.add(u);
			}
		
		}
    	catch (SQLException e) {
			e.printStackTrace();
    	} finally {
    		try {
				conn.close();
    		} catch (SQLException e) {
				e.printStackTrace();
    		} finally {
    			try {
    				conn.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}
    		}
    	}
		return uha;
	}

//	TO FIX THIS ONE--->
	public ArrayList<UserHasAttributes> getUsersHasAttrByUser(String user) {

		ArrayList<UserHasAttributes> uha = new ArrayList<UserHasAttributes>();

		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;
		try {
			String query = "select username,attribute from Users_has_attributes where username = ?";
			st = conn.prepareStatement(query);
			st.setString(1, user);
			res = st.executeQuery();
			while (res.next()) {
				UserHasAttributes u = new UserHasAttributes();
				u.setUsername(res.getString("username"));
				u.setAttribute(res.getString("attribute"));
				uha.add(u);
			}
		
		}
    	catch (SQLException e) {
			e.printStackTrace();
    	} finally {
    		try {
				conn.close();
    		} catch (SQLException e) {
				e.printStackTrace();
    		} finally {
    			try {
    				conn.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}
    		}
    	}
    	    	
		return uha;
	}

	
	// For debugging
	public void printAllUsersHasAttr() {
		
		ArrayList<UserHasAttributes> uha = getAllUsersHasAttr();

		System.out.println("+++++++++++all user-attribute pairs:");
		for (int i=0; i<uha.size(); i++) {
			System.out.println("++user:"+uha.get(i).getUsername());
			System.out.println("attr:"+uha.get(i).getAttribute());
		}
	}

	
	public void deleteUserHasAttr(String username, String attr) {
		
    	DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();

    	try {
			st = conn.prepareStatement("DELETE FROM Users_has_attributes WHERE username = ? AND attribute = ?");
	    	st.setString(1, username);
	    	st.setString(2, attr);
	    	st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void deleteAllUsersHasAttr() {
		ArrayList<UserHasAttributes> uha = getAllUsersHasAttr();

		System.out.println("[WARN]+++Deleting everything from user-has-attributes");

		for (int i=0; i<uha.size(); i++) {
			deleteUserHasAttr(uha.get(i).getUsername(), uha.get(i).getAttribute());			
		}
		
	}
	

	public void deleteAllUsersHasAttrByUser(String user) {

		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();

    	try {
			st = conn.prepareStatement("DELETE FROM Users_has_attributes WHERE username = ? ");
	    	st.setString(1, user);
	    	st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		
	}	

	public void deleteAllUsersHasAttrByAttr(String attr) {
		
		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
    	
    	try {

    		ResultSet res = null;
    		boolean recordExists = false;

    		//checking if Users_has_attributes record record exists
        	PreparedStatement st2;
			String query = "select attribute from Users_has_attributes where attribute = ?";
			st2 = conn.prepareStatement(query);
			st2.setString(1, attr);
			res = st2.executeQuery();
			
    		st = conn.prepareStatement("DELETE FROM Users_has_attributes WHERE attribute = ? ");
	    	st.setString(1, attr);
	    	st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		
	}
	
}
