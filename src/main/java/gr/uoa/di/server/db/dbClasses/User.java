package gr.uoa.di.server.db.dbClasses;

public class User {

	private String username;
	private String password;
	private String email;
	private String loggedin;

	public User(String username, String password, String email, String loggedin)
	{
		this.username = username;
		this.password = password;
		this.email 	  = email;
		this.loggedin = loggedin;		
	}

	public User()
	{
		this.username = "";
		this.password = "";
		this.email = "";
		this.loggedin = "no";		
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setLoggedin(String loggedin) {
		this.loggedin = loggedin;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	
	
	public String getUsername() {
		return username;
	}
	public String getPassword() {
		return password;
	}
	public String getLoggedin() {
		return loggedin;
	}
	public String getEmail() {
		return email;
	}
}
