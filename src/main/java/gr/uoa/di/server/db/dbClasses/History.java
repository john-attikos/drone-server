package gr.uoa.di.server.db.dbClasses;

public class History {

	private int id;
	private String username;
	private String attribute;
	private String date;
	private String value;
	
	public void setValue(String value) {
		this.value = value;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	
	public String getValue() {
		return value;
	}
	public int getId() {
		return id;
	}
	public String getDate() {
		return date;
	}
	public String getAttribute() {
		return attribute;
	}
	
	public String getUsername() {
		return username;
	}
	
}
