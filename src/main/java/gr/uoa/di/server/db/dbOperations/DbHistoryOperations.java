package gr.uoa.di.server.db.dbOperations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import gr.uoa.di.server.db.dbClasses.History;
import gr.uoa.di.server.main.MyLogger;

public class DbHistoryOperations {


		
	
	public void insertHistory(String username, String attribute, String value) {

		MyLogger.printi("Adding history ( " + username + " - " + attribute + " - " + value + " )");
		
		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
    	
    	DbUserHasAttributesOperations uhaOp = new DbUserHasAttributesOperations();
    	uhaOp.insertUserHasAttributes(username, attribute);
		
        Long tsLong = System.currentTimeMillis();
        String ts = tsLong.toString();
    	
		try {
			st = conn.prepareStatement("INSERT INTO History (username, attribute, date, value) VALUES (?, ?, ?, ?)");
			st.setString(1, username);
			st.setString(2, attribute);
			st.setString(3, ts);
			st.setString(4, value);
			st.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public ArrayList<History> getAllHistory() {
		ArrayList<History> history = new ArrayList<History>();
	
		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;
		try {
			//checking if UserName exists
			String query = "select * from History";
			st = conn.prepareStatement(query);
			res = st.executeQuery();
			while (res.next()) {
				History h = new History();
				h.setId(res.getInt("id"));
				h.setAttribute(res.getString("attribute"));
				h.setUsername(res.getString("username"));
				h.setDate(res.getString("date"));
				h.setValue(res.getString("value"));
				history.add(h);
			}
		
		}
    	catch (SQLException e) {
			e.printStackTrace();
    	} finally {
    		try {
				conn.close();
    		} catch (SQLException e) {
				e.printStackTrace();
    		} finally {
    			try {
    				conn.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}
    		}
    	}
		return history;
	}

	
	public ArrayList<History> getHistoryByUser(String user)
	{
		ArrayList<History> allHistory = getAllHistory();

		for (int i=0; i<allHistory.size(); i++)
		{
			if ( !allHistory.get(i).getUsername().toString().equals(user) )
			{
				allHistory.remove(i);
			}
		}
		
		return allHistory;
	}
	
	
	// For debugging
	public void printAllHistory() {
		
		ArrayList<History> history = getAllHistory();

		for (int i=0; i<history.size(); i++) {
			System.out.println("+++++++++++");
			System.out.println(history.get(i).getId());
			System.out.println(history.get(i).getAttribute());
			System.out.println(history.get(i).getUsername());
			System.out.println(history.get(i).getDate());
			System.out.println(history.get(i).getValue());			
		}
	}

	
	public void deleteHistoryByUser(String username) {
		
    	DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();

    	try {
			st = conn.prepareStatement("DELETE FROM History WHERE username = ? ");
	    	st.setString(1, username);
	    	st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	
	public void deleteHistoryByAttribute(String attr) {
		
    	DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();

    	try {
			st = conn.prepareStatement("DELETE FROM History WHERE attribute = ? ");
	    	st.setString(1, attr);
	    	st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void deleteAllHistory() {
		ArrayList<History> history = getAllHistory();

		System.out.println("+++Deleting history");

		for (int i=0; i<history.size(); i++) {
			deleteHistoryByUser(history.get(i).getUsername());
			
		}
	}
	

	
}
