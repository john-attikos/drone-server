package gr.uoa.di.server.db.dbOperations;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import gr.uoa.di.server.db.dbClasses.Attribute;
import gr.uoa.di.server.main.MyLogger;


public class DbAttributeOperations {


	public static boolean checkAttributeExistance(String name) {
		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;
		try {
			//checking if Name exists
			String query = "select * from Attributes where name = ?";
			st = conn.prepareStatement(query);
			st.setString(1, name);
			res = st.executeQuery();
			if (res.next())
				return true;
			else
				return false;
    	}
    	catch (SQLException e) {
			e.printStackTrace();
    	} finally {
    		try {
				conn.close();
    		} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
		return false;
	}

		
	
	public void insertAttribute(String name) {
		if (checkAttributeExistance(name))
			return;

		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;


		try {
			st = conn.prepareStatement("INSERT INTO Attributes (name) VALUES (?)");
			st.setString(1, name);
			st.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public ArrayList<Attribute> getAllAttributes() {
		ArrayList<Attribute> attr = new ArrayList<Attribute>();
	
		DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();
		ResultSet res = null;
		try {
			//checking if UserName exists
			String query = "select * from Attributes";
			st = conn.prepareStatement(query);
			res = st.executeQuery();
			while (res.next()) {
				Attribute a = new Attribute();
				a.setName(res.getString("name"));
				attr.add(a);
			}
		
		}
    	catch (SQLException e) {
			e.printStackTrace();
    	} finally {
    		try {
				conn.close();
    		} catch (SQLException e) {
				e.printStackTrace();
    		} finally {
    			try {
    				conn.close();
    			} catch (SQLException e) {
    				e.printStackTrace();
    			}
    		}
    	}
		return attr;
	}

	
	// For debugging
	public void printAllAttributes() {
		
		ArrayList<Attribute> attr = getAllAttributes();

		System.out.println("+++++++++++attributes");
		for (int i=0; i<attr.size(); i++) {
			System.out.println("name:"+attr.get(i).getName());
			
		}
	}

	
	public void deleteAttribute(String name) {
		
    	DbCreation db = DbCreation.getInstance();
        Connection conn;
    	PreparedStatement st;
    	conn = db.connect();

    	MyLogger.printi("> Deleting attribute: " + name);
    	
    	// Due to the relation of the tables, when deleting an Attribute we should
    	// also delete the User_has_Attributes records which refer to it
    	// (same goes for History table)
    	DbUserHasAttributesOperations uha = new DbUserHasAttributesOperations();
		uha.deleteAllUsersHasAttrByAttr(name);

		DbHistoryOperations history = new DbHistoryOperations();
		history.deleteHistoryByAttribute(name);
		
    	try {
			st = conn.prepareStatement("DELETE FROM Attributes WHERE name = ?");
	    	st.setString(1, name);
	    	st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
    	
	}

	public void deleteAllAttributes() {
				
		ArrayList<Attribute> attr = getAllAttributes();

		MyLogger.printi("Deleting all attributes");

		for (int i=0; i<attr.size(); i++) {
			deleteAttribute(attr.get(i).getName());
			
		}
		
	}
	
}
