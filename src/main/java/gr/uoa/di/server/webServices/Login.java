package gr.uoa.di.server.webServices;


import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import gr.uoa.di.server.db.dbOperations.DbUserOperations;
import gr.uoa.di.server.main.MyLogger;


@Path("security")
public class Login {

	ObjectMapper mapper = new ObjectMapper();

    @GET
    @Path("/login")
    public void login(final @HeaderParam("user") String user, final @HeaderParam("pass") String pass, @Suspended final AsyncResponse asyncResponse )
    {

		new Thread(new Runnable() {
			
			@Override
			public void run(){
			
				MyLogger.printi("Someone is trying to login with:\n\tun:" + user + "\n\tpw:" + pass);

				Boolean existance = DbUserOperations.tryLogin(user, pass);

				String response = new String();

				try {
					response = mapper.writeValueAsString(existance);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}

				MyLogger.printi("User '" + user + "' login request returned: " + response);
				
				asyncResponse.resume(response);
			}
		}).start();

    }
    
    
    
    
    
}
