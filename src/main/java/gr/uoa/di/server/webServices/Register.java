package gr.uoa.di.server.webServices;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import gr.uoa.di.server.db.dbClasses.Attribute;
import gr.uoa.di.server.db.dbOperations.DbAttributeOperations;
import gr.uoa.di.server.db.dbOperations.DbHistoryOperations;
import gr.uoa.di.server.db.dbOperations.DbUserHasAttributesOperations;
import gr.uoa.di.server.db.dbOperations.DbUserOperations;
import gr.uoa.di.server.main.MyLogger;


@Path("security")
public class Register {
	ObjectMapper mapper = new ObjectMapper();

    @GET
    @Path("/register")
    public void register(final @HeaderParam("user") String user, final @HeaderParam("pass") String pass, final @HeaderParam("email") String email, final @HeaderParam("userAttributes") String userAttributes, @Suspended final AsyncResponse asyncResponse )
    {

		new Thread(new Runnable() {
			
			@Override
			public void run(){
			
				MyLogger.printi("Someone is trying to register with:\n\tun: " + user + "\n\tpw: " + pass + "\n\temail: " + email + "\n\tuserAttributes: " + userAttributes);
								
				Boolean uniqueUser = !DbUserOperations.checkUserExistance(user);

				String response = new String();

				// If good to register, let's do it! (email and password format validation happens on ANDROID)
				if ( uniqueUser )
				{
					DbUserOperations usersOperations = new DbUserOperations();
					DbUserHasAttributesOperations uhaOperations = new DbUserHasAttributesOperations();

					usersOperations.insertUser(user, pass, email);
					
					// New user's attributes come from ANDROID in a comma separated String
					List<String> userAttributesList = Arrays.asList(userAttributes.split("\\s*,\\s"));

					for (int i=0; i<userAttributesList.size(); i++)
					{
						uhaOperations.insertUserHasAttributes(user, userAttributesList.get(i));
					}
				}
				
				
				try {
					response = mapper.writeValueAsString(uniqueUser);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}

				MyLogger.printi("New user '" + user + "' register request returned: " + response);
				
				asyncResponse.resume(response);
			}
		}).start();

    }
    
    
    
    
    
}
