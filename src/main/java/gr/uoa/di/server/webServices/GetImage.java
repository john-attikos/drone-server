package gr.uoa.di.server.webServices;

import java.io.FileNotFoundException;

import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.databind.ObjectMapper;

import gr.uoa.di.server.db.dbClasses.Image;
import gr.uoa.di.server.db.dbOperations.DbImageOperations;
import gr.uoa.di.server.main.MyLogger;

@Path("image-service")
public class GetImage {

	ObjectMapper mapper = new ObjectMapper();
    
	
	@POST
	public void imageReceiver(final @HeaderParam("user") String user,String jsonString)
	{

		
		JSONParser parser = new JSONParser();
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject = (JSONObject) parser.parse(jsonString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		Image image = new Image();
		
		image.setEncoded(jsonObject.get("encoded").toString());
		image.setFileName(user + "_" + jsonObject.get("fileName").toString());
		
		
		MyLogger.printd("");
		MyLogger.printd("");
		MyLogger.printd("encoded: " + image.getEncoded());
		MyLogger.printd("User: " + user);
		MyLogger.printd("file name: " + jsonObject.get("fileName"));
		MyLogger.printd("");
		MyLogger.printd("");
		MyLogger.printd("");

		image.imageToFile();

		DbImageOperations imageOp = new DbImageOperations();
		imageOp.insertImageRecord(image.getFileName());
		
		imageOp.printAllImages();
				
	}
	
    
    
    
}
