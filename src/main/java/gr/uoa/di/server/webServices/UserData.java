package gr.uoa.di.server.webServices;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
//import com.google.gwt.user.client.History;

import gr.uoa.di.server.db.dbClasses.Attribute;
import gr.uoa.di.server.db.dbClasses.History;
import gr.uoa.di.server.db.dbClasses.Image;
import gr.uoa.di.server.db.dbClasses.UserHasAttributes;
import gr.uoa.di.server.db.dbOperations.DbAttributeOperations;
import gr.uoa.di.server.db.dbOperations.DbHistoryOperations;
import gr.uoa.di.server.db.dbOperations.DbUserHasAttributesOperations;
import gr.uoa.di.server.main.DummyDrone;
import gr.uoa.di.server.main.MyLogger;


@Path("data")
public class UserData {

	ObjectMapper mapper = new ObjectMapper();

    @GET
    @Path("/history")
    public void sendHistory(final @HeaderParam("user") String user, @Suspended final AsyncResponse asyncResponse )
    {

		new Thread(new Runnable() {
			
			@Override
			public void run(){

				MyLogger.printi("User '" + user + "' requested his history");

				DbHistoryOperations historyOp = new DbHistoryOperations();

//				historyOp.printAllHistory();				
				
				String jsonInString = new String();

//				ArrayList<History> history = new ArrayList<>();
//				history = historyOp.getHistoryByUser(user);

				ArrayList<History> history = historyOp.getHistoryByUser(user);
				
				try {
					jsonInString = mapper.writeValueAsString(history);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				
				
				asyncResponse.resume(jsonInString);

			}
		}).start();

    }
    
    
    // Returns all the attributes (is used on registration of Android side)
    @GET
    @Path("/attributes")
    public void sendAttributes(final @HeaderParam("user") String user, @Suspended final AsyncResponse asyncResponse )
    {

		new Thread(new Runnable() {
			
			@Override
			public void run(){

				MyLogger.printi("User '" + user + "' requested his attributes");

				DbUserHasAttributesOperations uhaOp = new DbUserHasAttributesOperations();
				
				String jsonInString = new String();

//				ArrayList<History> history = new ArrayList<>();
//				history = historyOp.getHistoryByUser(user);

				ArrayList<UserHasAttributes> uha = uhaOp.getUsersHasAttrByUser(user);
				
				try {
					jsonInString = mapper.writeValueAsString(uha);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				
				
				asyncResponse.resume(jsonInString);

			}
		}).start();

    }
    
    

    // sendAllAttributes() is used only on new user registration
    @GET
    @Path("/all-attributes")
    public void sendAllAttributes(final @HeaderParam("user") String user, @Suspended final AsyncResponse asyncResponse )
    {

		new Thread(new Runnable() {
			
			@Override
			public void run(){

				String jsonInString = new String();
				
				DbAttributeOperations attrOp = new DbAttributeOperations();

				ArrayList<Attribute> attributes= attrOp.getAllAttributes();;

				ArrayList<String> allAttributesString = new ArrayList<>();

				// For simplicity, Android client will expect array of strings
				for (int i=0; i<attributes.size(); i++)
				{
					allAttributesString.add(attributes.get(i).getName());
				}
				
				try {
					jsonInString = mapper.writeValueAsString(attributes);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}

				asyncResponse.resume(jsonInString);

			}
		}).start();

    }
    

    
    @GET
    @Path("/attributes-logos")
    public void sendAttributesLogos(final @HeaderParam("user") String user, @Suspended final AsyncResponse asyncResponse )
    {

		new Thread(new Runnable() {
			
			@Override
			public void run(){

				MyLogger.printi("User '" + user + "' requested his attributes' logos");

				DbUserHasAttributesOperations uhaOp = new DbUserHasAttributesOperations();
				
				String jsonInString = new String();

				ArrayList<UserHasAttributes> uha = uhaOp.getUsersHasAttrByUser(user);
				
				ArrayList<Image> images = new ArrayList<>();

				for (int i=0; i<uha.size(); i++)
				{
					MyLogger.printd(uha.get(i).getAttribute());
					Image im = new Image();
					Attribute attr = new Attribute();
					im.setFileName(attr.getLogoByAttribute(uha.get(i).getAttribute()));
					im.setEncoded(im.imageToString(im.getFileName()));
					images.add(im);
				}
				
				for (int i=0; i<images.size(); i++)
				{
					System.out.println("Sending image: " + images.get(i).getFileName());
//					System.out.println("Encoded: " + images.get(i).getEncoded());
//					System.out.println("\n---------Sending logo No " + i + "-------------");
//					System.out.println("\n\n------length = " + images.get(i).getEncoded().length() + "\n\n");
				}
				
				try {
					jsonInString = mapper.writeValueAsString(images);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				
				
				asyncResponse.resume(jsonInString);

			}
		}).start();

    }
    

    @GET
    @Path("/drone-images")
    public void sendUserDroneImages(final @HeaderParam("user") String user, @Suspended final AsyncResponse asyncResponse )
    {

		new Thread(new Runnable() {
			
			@Override
			public void run(){

				MyLogger.printi("User '" + user + "' requested his drone images");

				String jsonInString = new String();

				ArrayList<String> userDroneImages = Image.getUserDroneImages(user);
				
				ArrayList<Image> images = new ArrayList<>();

				for (int i=0; i<userDroneImages.size(); i++)
				{
					MyLogger.printd(userDroneImages.get(i));
					Image im = new Image();
					String imageName = new String();
//					Attribute attr = new Attribute();
//					im.setFileName(attr.getLogoByAttribute(userDroneImages.get(i).getAttribute()));
					im.setFileName(userDroneImages.get(i));
					im.setEncoded(im.droneImageToString(im.getFileName()));
					images.add(im);
				}
				
				for (int i=0; i<images.size(); i++)
				{
					System.out.println("Sending image: " + images.get(i).getFileName());
				}
				
				try {
					jsonInString = mapper.writeValueAsString(images);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
				
				
				asyncResponse.resume(jsonInString);

			}
		}).start();

    }
    

    
    @GET
    @Path("/drone-condition")
    public void sendDroneCondition(final @HeaderParam("user") String user, @Suspended final AsyncResponse asyncResponse )
    {

		new Thread(new Runnable() {
			
			@Override
			public void run(){
				
				MyLogger.printi("User '" + user + "' requested drone's condition");

				String jsonInString = new String();
				/////////////////////////////////////////////////////////////////////////
				// NOTE: This is a dummy function since we don't actually have a drone //
				/////////////////////////////////////////////////////////////////////////

				ArrayList<DummyDrone> drone = new ArrayList<>();
				DummyDrone d = new DummyDrone();

				d.setBatteryState(66);
				d.setDistanceFromBase(11);
				d.setFlightTime(22);
				
				drone.add(d);
				
				try {
					jsonInString = mapper.writeValueAsString(drone);
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				}
								
				asyncResponse.resume(jsonInString);

			}
		}).start();

    }
    
    
    
}
